# Washing Machine 
- Real-time automatic washing machine with washing time countdown and four programs. 

# LIB:
- datetime 
- sys

# Install

- extract all files to new directory
- open console and go to directory
- enter ./install.sh

# Uninstall

- open console and go to directory
- enter ./uninstall.sh


# How to Run it:

- open console
- enter [username@fedora ~]$ washingmachine


# License:

- opensource