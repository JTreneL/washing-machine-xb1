from setuptools import setup, find_packages

setup(
    name='washingmachine',  # Required
    version='1.0',  # Required
    url='https://gitlab.com/JTreneL/washing-machine-xb1',  # Optional
    author='JTrenel',  # Optional
    author_email='Janlenert@email.cz',  # Optional
    packages=find_packages(),  # Required
    entry_points={"console_scripts": ["washingmachine = washingmachine.washingmachine:main"]},
    
)
